﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Stomatology
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EntryCreatingPage : ContentPage
	{
        string specialistId;
        string date;
        string time;
        string note;
        Button createEntryButton;
        DateTime minimumDate;
        Picker specialistPicker;
        DatePicker datePicker;
        Picker timePicker;
        Image rightArrow;
        StackLayout specialistPickerLayout;
        StackLayout datePickerLayout;
        StackLayout timePickerLayout;
        StackLayout layout;
        List<SpecialistItem> specialistList;
        public EntryCreatingPage ()
		{
            note = "";
            specialistList = new List<SpecialistItem>();
			InitializeComponent ();
            createEntryButton = new Button
            {
                IsEnabled = false,
                BackgroundColor = Color.White,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Text = "Записаться",
                TextColor = Color.Gray,
                Margin=30
            };
            //TODO: recieve minimum date from server?
            minimumDate = DateTime.Now;
            specialistPicker = new Picker
            {
                Title = "Выбор врача",
                BackgroundColor = Color.White,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                ItemsSource = GetSpecialists()
            };
            specialistPicker.SelectedIndexChanged += SpecialistChosen;
            datePicker = new DatePicker
            {
                IsEnabled=false,
                Format = "D",
                HorizontalOptions = LayoutOptions.FillAndExpand,
                TextColor = Color.Gray,
                MinimumDate = minimumDate
            };
            datePicker.PropertyChanged += DateChosen;
            timePicker = new Picker
            {
                IsEnabled = false,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                TextColor = Color.Gray
            };
            timePicker.SelectedIndexChanged += TimeChosen;
            createEntryButton.Clicked += ButtonClick;
            rightArrow = new Image
            {
                Source = "arrowRight.png",
                Scale = 0.5f
            };
            specialistPickerLayout = new StackLayout
            {
                BackgroundColor = Color.White,
                HeightRequest = 50,
                Orientation = StackOrientation.Horizontal,
                Children =
                {
                    specialistPicker,
                    rightArrow
                }
            };
            datePickerLayout = new StackLayout
            {
                BackgroundColor = Color.White,
                HeightRequest = 50,
                Orientation = StackOrientation.Horizontal,
                Children =
                {
                    datePicker,
                    rightArrow
                }
            };
            timePickerLayout = new StackLayout
            {
                BackgroundColor = Color.White,
                HeightRequest = 50,
                Orientation = StackOrientation.Horizontal,
                Children =
                {
                    timePicker,
                    rightArrow
                }
            };
            layout = new StackLayout
            {
                Margin = 40,
                Padding = 20,
                BackgroundColor = Color.Transparent, ///Must be transparent
                VerticalOptions = LayoutOptions.CenterAndExpand,
                Children =
                {
                    specialistPickerLayout,
                    datePickerLayout,
                    timePickerLayout,
                    createEntryButton
                }
            };
            Content = new ScrollView
            {
                Content = layout
            };
            BackgroundImage = "background.png";
            Title = "Запись на прием";
        }
        private List<string> GetSpecialists()
        {
            List<string> result = new List<string>();
            JToken jsonResponse = AllSpecialists();
            if (jsonResponse[Globals.TYPE_KEY].ToString() == "ok")
            {
                JArray files = (JArray)jsonResponse[Globals.RESPONSE_KEY];
                foreach (JToken file in files)
                {
                    string description =
                        file["id"].ToString() + ", " +
                        file["surname"].ToString() + ", " +
                        file["firstname"].ToString() + ", " +
                        file["patronymic"].ToString() + ", " +
                        file["sex"].ToString() + ", " +
                        file["note"].ToString();
                    result.Add(file["surname"].ToString() + " " + file["firstname"].ToString());
                    specialistList.Add(new SpecialistItem
                    {
                        id = file["id"].ToString(),
                        firstname = file["firstname"].ToString(),
                        surname = file["surname"].ToString(),
                        patronymic = file["patronymic"].ToString(),
                        sex = file["sex"].ToString(),
                        note = file["note"].ToString()
                    });
                }
            }
            return result;
        }
        private JToken AllSpecialists()
        {
            string response = Globals.PostRequest(Globals.SpecialistAll_ApiUri);
            JToken jsonResponse = JToken.Parse(response);
            return jsonResponse;
        }
        private List<string> GetAvailableTime(string specialistId, string date)
        {
            string uri = Globals.GetAvailableTime_ApiUri(specialistId, date);
            string response = Globals.PostRequest(uri);
            JToken jsonResponse = JToken.Parse(response);
            List<string> result = new List<string>();
            if (jsonResponse[Globals.TYPE_KEY].ToString() == "ok")
            {
                jsonResponse = JToken.Parse(jsonResponse[Globals.RESPONSE_KEY].ToString());
                var values = jsonResponse["time_entry"].Values();
                foreach (var value in values)
                {
                    result.Add(value.ToString());
                }
            }
            return result;
        }
        private void SpecialistChosen(object sender, EventArgs e)
        {
            string specialist = (string)specialistPicker.SelectedItem;
            foreach(SpecialistItem s in specialistList)
            {
                if (specialist == s.surname + " " + s.firstname)
                {
                    specialistId = s.id;
                }
            }
            
            datePicker.IsEnabled = true;
            timePicker.IsEnabled = false;
            createEntryButton.IsEnabled = false;
            
        }
        private void DateChosen(object sender, EventArgs e)
        {
            date = datePicker.Date.ToString("yyyy-MM-dd");
            Debug.Print("Date: yyyy {" + datePicker.Date.Year + "} mm {" + datePicker.Date.Month + "} day {" + datePicker.Date.Day + "}");
            Debug.Print("Date = " + date + "; SpecialistId = " + specialistId);
            timePicker.ItemsSource = GetAvailableTime(specialistId, date);

            if (specialistPicker.SelectedIndex != -1)
            {
                timePicker.IsEnabled = true;
                createEntryButton.IsEnabled = false;
            }
            
        }
        private void TimeChosen(object sender, EventArgs e)
        {
            time = (string)timePicker.SelectedItem;
            
            createEntryButton.IsEnabled = true;
            
        }
        private void ButtonClick(object sender, EventArgs e)
        {
            string uri = Uri.EscapeUriString(Globals.CreateEntry_ApiUri(specialistId, date, time));
            string response = Globals.PostRequest(uri);
            Debug.Print("URI: " + uri);
            Debug.Print("Create: " + response);
        }
    }
}
