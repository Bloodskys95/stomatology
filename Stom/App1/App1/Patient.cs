﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stomatology
{
    public class Patient
    {
        public Patient()
        {
            firstname = string.Empty;
            surname = string.Empty;
            patronymic = string.Empty;
            pasport = string.Empty;
            insurance = string.Empty;
            sex = string.Empty;
            birth = string.Empty;
        }
        public string firstname;
        public string surname;
        public string patronymic;
        public string pasport;
        public string insurance;
        public string sex;
        public string birth;
    }
}
