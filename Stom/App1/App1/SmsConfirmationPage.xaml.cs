﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Stomatology
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SmsConfirmationPage : ContentPage
	{
		public SmsConfirmationPage ()
		{
			InitializeComponent ();
		}

        public void OnClick(object sender, EventArgs e)
        {
            NameValueCollection data = new NameValueCollection();
            data.Add("sms_password", smsEntry.Text);
            string result = Globals.PostRequest(Globals.Sms_ApiUri, data);
            JObject response = JObject.Parse(result);
            if (response.Value<string>("type") == "ok")
            {
                UpdateToken(response.GetValue(Globals.ACCESS_TOKEN_KEY).ToString());
                Globals.RaiseLoginHandler();
            }
        }

        public void UpdateToken(string token)
        {
            Globals.TOKEN = token;
        }
    }
}
