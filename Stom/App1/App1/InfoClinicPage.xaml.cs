﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Syncfusion.SfCarousel.XForms;
using Syncfusion.SfRotator.XForms;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Collections.ObjectModel;

namespace Stomatology
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InfoClinicPage : ContentPage
    {
        public InfoClinicPage()
        {
            InitializeComponent();
            SfCarousel carousel1 = new SfCarousel { ItemWidth = 300, ItemHeight = 250,SelectedIndex=1 , RotationAngle =45, Offset =30, SelectedItemOffset =2};
            ObservableCollection<SfCarouselItem> collectionOfItems1 = new ObservableCollection<SfCarouselItem>();
            collectionOfItems1.Add(new SfCarouselItem() { ImageName = "photo1.jpg" });
            collectionOfItems1.Add(new SfCarouselItem() { ImageName = "photo2.jpg" });
            collectionOfItems1.Add(new SfCarouselItem() { ImageName = "photo3.jpg" });
            carousel1.DataSource = collectionOfItems1;
          
            /*
            SfCarousel carousel2 = new SfCarousel { ItemWidth = 300, ItemHeight = 400, SelectedIndex = 1, RotationAngle = 45, Offset = 30, SelectedItemOffset = 2 };
            ObservableCollection<SfCarouselItem> collectionOfItems2 = new ObservableCollection<SfCarouselItem>();
            collectionOfItems2.Add(new SfCarouselItem() { ImageName = "photo1.jpg" });
            collectionOfItems2.Add(new SfCarouselItem() { ImageName = "Serf2.jpg" });
            collectionOfItems2.Add(new SfCarouselItem() { ImageName = "Serf3.jpg" });
            carousel2.DataSource = collectionOfItems2;
            */
            Image Banner = new Image
            {
                Source = "bannerNew.png"
            };

            Button navigator = new Button
            {
                Margin =-5,
                BackgroundColor =Color.White,
                Text = "пр. Науки, 38",
                TextColor =Color.Gray,
                TranslationY =-25,
                VerticalOptions =LayoutOptions.CenterAndExpand
            };            
            navigator.Clicked += OpenNavigator_Clicked;
            navigator.Image =  "placeholderFieldPoint.png" ;

            Button ReviewCl = new Button
            {
                BackgroundColor = Color.White,
                Text = "Отзывы о клинике",
                TextColor = Color.Gray
            };
            ReviewCl.Clicked += ReviewClinic;

            Button Price = new Button
            {
                BackgroundColor = Color.White,
                Text = "Прайс лист",
                TextColor = Color.Gray
            };
            Price.Clicked += OpenPrice_Clicked;

            Button Spec = new Button
            {
                BackgroundColor = Color.White,
                Text = "Наши специалисты",
                TextColor = Color.Gray
            };
            Spec.Clicked += OpenSpec_Clicked;

            Label Info = new Label
            {
                Margin =10,
                HorizontalOptions =LayoutOptions.Center,
                HorizontalTextAlignment =TextAlignment.Center,
                Text = "Стоматологические клиники Санкт-Петербурга предлагают своим клиентам весь спектр востребованных услуг на самом высоком уровне. И наша клиника на сегодняшний день оснащена самым прогрессивным оборудованием, использует в работе материалы превосходного качества, внедряет в практику последние новшества стоматологии. «СолоДент» — известный и уважаемый бренд в своем сегменте рынка.",
                TextColor =Color.Gray
            };

            StackLayout labelStack = new StackLayout
            {
                BackgroundColor = Color.White,
                Children ={ Info }
            };

            StackLayout mainStack = new StackLayout
            {
                Children =
                {
                    Banner,
                    carousel1,
                    navigator,
                    ReviewCl,
                    Price,
                    Spec ,
                    labelStack,
                    /*carousel2*/
                },
                Spacing =3
            };
            Content = new ScrollView { Content = mainStack };
        }

        private void MainCarouselView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {


        }


        private void OpenNavigator_Clicked(object sender, EventArgs e)
        {
            var address = "пр. Науки, 38";

            switch (Device.OS)
            {
                case TargetPlatform.iOS:
                    Device.OpenUri(
                    new Uri(string.Format("http://maps.apple.com/?q={0}", WebUtility.UrlEncode(address))));
                    break;
                case TargetPlatform.Android:
                    Device.OpenUri(
                    new Uri(string.Format("geo:0,0?q={0}", WebUtility.UrlEncode(address))));
                    break;
            }
        }

        public async void OpenPrice_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PricePage());
        }

        public async void OpenSpec_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SpecList());
        }
        public async void ReviewClinic(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ReviewClinicPage());
        }
    }
}