﻿using Newtonsoft.Json.Linq;
using Plugin.Messaging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Stomatology
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PricePage : ContentPage
    {

        StackLayout ScrollServiseLayout = new StackLayout()
        {        
            TranslationY=5,
        Orientation = StackOrientation.Vertical
         };
        //Клас услуги с пунктами услуги и их ценой
        public class ServicePriceItem
        {
            public string ItemService;
            public string CostItemServise;
        }
        public class ServicePriceSection
        {
            public String sectionName;
            public List<ServicePriceItem> ServicePriceItemsList = new List<ServicePriceItem>() { };
        }

        //Лист услуг с пунктами услуги и их ценой 
        List<ServicePriceSection> ServicePriceList;

        public void ItemsAddList(string section, List<ServicePriceItem> items)
        {
            ServicePriceSection F = new ServicePriceSection();
            F.sectionName = section;
            for (int i = 0; i < items.Count; i++)
            {
                F.ServicePriceItemsList.Add(items[i]);
            }
            ServicePriceList.Add(F);
        }


        public PricePage ()
		{
			InitializeComponent ();
            ServicePriceList = GetPrice();
            //Вызов функции добавления элементов на экран
            AdditemService(ServicePriceList);
            //***************
            //нижняя кнопка звонка
            Button Coll = new Button()
            {
                Text ="Вы не нашли в прайсе стоимость необходимой услуги или у вас есть дополнительные вопросы?",
                TextColor = Color.Gray,
                Image = "phone.png",
                HorizontalOptions =LayoutOptions.FillAndExpand,
                BackgroundColor = Color.White
            };
            Coll.Clicked += Coll_Clicked;
            //Добавления ScrollView
            ScrollView ScrollViewContent = new ScrollView { Content = ScrollServiseLayout };
            //Расположение кнопки в низу
            StackLayout FooterLayout = new StackLayout()
            {
                Children = {Coll },
                VerticalOptions = LayoutOptions.EndAndExpand
            };
            //обединение всех StackLayout
            StackLayout MainServiseLayout = new StackLayout()
            {
                Orientation = StackOrientation.Vertical,
                Children = { ScrollViewContent, FooterLayout }
            };
            //Вывод на экран
            Content = MainServiseLayout;
        }

        private void Coll_Clicked(object sender, EventArgs e)
        {
            //Globals.PostRequest("http://prostopost.com/api/back_call_add",Prof_Dan.);
            //****XAM.PLUGINS.MASAGING
            var phoneDialer = CrossMessaging.Current.PhoneDialer; if (phoneDialer.CanMakePhoneCall) phoneDialer.MakePhoneCall("+8 800 555 35 35");
        }

        private List<ServicePriceSection> GetPrice()
        {
            string response = Globals.PostRequest(Globals.Price_ApiUri);
            JToken jsonResponse = JToken.Parse(response);
            List<ServicePriceSection> priceList = new List<ServicePriceSection>();
            if (jsonResponse[Globals.TYPE_KEY].ToString() == "ok")
            {
                JArray sections = (JArray)jsonResponse[Globals.RESPONSE_KEY];
                foreach (JToken section in sections)
                {
                    ServicePriceSection s = new ServicePriceSection()
                    {
                        sectionName = section["section"]["name"].ToString().ToUpper(),
                        ServicePriceItemsList = new List<ServicePriceItem>()
                    };
                    foreach (var services in section["services"])
                    {
                        ServicePriceItem item = new ServicePriceItem()
                        {
                            ItemService = services["name"].ToString(),
                            CostItemServise = services["cost"].ToString() + " Р."
                        };
                        s.ServicePriceItemsList.Add(item);
                    }
                    priceList.Add(s);
                }
            }
            return priceList;
        }
        //добавление услуг с пунктами услуги и их ценой на StackLayout
        public void AdditemService(List<ServicePriceSection> ServicePriceList)
        {
            StackLayout ServiseLayout = new StackLayout()
            {
                Orientation = StackOrientation.Vertical,
                BackgroundColor = Color.White,
                Padding = 20
            };
            for (int i = 0; i < ServicePriceList.Count; i++)
            {
                Label NameServiceEntry = new Label()
                {
                    HorizontalOptions = LayoutOptions.StartAndExpand,
                    TextColor = Color.Purple,
                    Text = ServicePriceList[i].sectionName
                };
                ServiseLayout.Children.Add(NameServiceEntry);

                for (int p = 0; p < ServicePriceList[i].ServicePriceItemsList.Count; p++)
                {
                    Label ItemServiseEntry = new Label()
                    {
                        HorizontalOptions = LayoutOptions.StartAndExpand,
                        TextColor = Color.Gray,
                        Text = ServicePriceList[i].ServicePriceItemsList[p].ItemService
                    };

                    Label CostItemServiseEntry = new Label()
                    {
                        HorizontalOptions = LayoutOptions.EndAndExpand,
                        TextColor = Color.Black,
                        Text = ServicePriceList[i].ServicePriceItemsList[p].CostItemServise
                    };

                    StackLayout ItemServiseLayout = new StackLayout()
                    {
                        Children = { ItemServiseEntry, CostItemServiseEntry },
                        Orientation = StackOrientation.Horizontal
                    };
                    ServiseLayout.Children.Add(ItemServiseLayout);
                }
                Image Line = new Image()
                {
                    Source = "line.png"
                    ,Scale=1.2
                };
                ServiseLayout.Children.Add(Line);
            }
            ScrollServiseLayout.Children.Add(ServiseLayout);
        }
    }
}
