﻿using Newtonsoft.Json.Linq;
using Plugin.Settings;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Net;
using System.Text;
using Xamarin.Forms;

namespace Stomatology
{
    public class Globals
    {

        private Globals()
        {
        }
        static Globals instance;
        static string SERVER_NAME = "http://api.prostopost.com";
        static string LOGIN_API =           "/api/guest/login";
        static string REGISTER_API =        "/api/guest/registration";
        static string PATIENT_GET_API = "/api/patient/patient_get";
        static string PATIENT_SET_API = "/api/patient/patient_set";
        static string VALIDATION_API =      "/api/patient/login_if";
        static string TEST_HEADER_API =     "/api/patient/header";
        static string SMS_API =             "/api/guest/sms";
        static string FILE_ALL_API =        "/api/patient/file_all";
        static string SPECIALIST_ALL_API = "/api/specialist_all";
        static string COMMENT_ALL_BY_SPECIALIST_API = "/api/specialist_comment_all_by_specialist";
        static string COMMENT_ALL_CLINIC_ANON = "/api/comment_all";
        static string COMMENT_ALL_CLINIC = "/api/comment_all";
        static string COMMENT_ADD_API = "/api/patient/specialist_comment_add";
        static string PRICE_API = "/api/price_get";
        static string COMMENT_CLINIC_ADD_API = "/api/patient/comment_add";
        static string GET_AVAILABLE_TIME_API = "/api/patient/time_day";
        static string GET_ALL_SPECIALISTS = "/api/patient/specialist_all";
        static string GET_ALL_THERAPIES = "/api/patient/therapy_all";
        static string GET_THERAPY = "/api/patient/therapy_get";
        static string GET_ALL_ENTRIES = "/api/patient/entry_all";
        static string GET_ENTRY = "/api/patient/entry_get";
        static string CREATE_ENTRY = "/api/patient/entry_add";
        static string BACK_CALL_ADD = "/api/patient/back_call_add";
        static string token;
        public static string ACCESS_TOKEN_KEY = "access_token";
        public static string RESPONSE_KEY = "response";
        public static string TYPE_KEY = "type";
        public static Patient ActiveUser;
        public static Page CurrentPage;
        public static EventHandler LoginHandler;
        public static EventHandler RegisterHandler;
        public static EventHandler LogoutHandler;

        /* Возможно есть смысл в методе PostRequest сделать первым аргументом enum (чтобы улучшить опыт разработки)
        public enum API
        {
            Login,
            Register,
            PatientGet,
            PatientSet,
            Validation,
            Sms,
            AllFiles,
            AllSpecialists
        }
        */

        public static Globals Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Globals();
                }
                return instance;
            }
        }
        public static bool IsTokenExistsAndValid()
        {
            if (Settings.Token != string.Empty)
            {
                token = Settings.Token;
            }
            string response = PostRequest(Validation_ApiUri);
            bool isValid = JToken.Parse(response)["type"].ToString() == "ok";
            string r = PostRequest(Validation_ApiUri);
            //RaiseLoginHandler();
            return isValid;
        }
        public static string PostRequest(string serverUrl)
        {
            return PostRequest(serverUrl, new NameValueCollection());
        }
        public static string PostRequest(string serverUrl, NameValueCollection dataList)
        {
            string result = "";
            using (WebClient client = new WebClient())
            {
                client.Headers.Add("Authorization", "Bearer " + Globals.TOKEN);
                result = new string(Encoding.UTF8.GetChars(client.UploadValues(serverUrl, dataList)));
            }
            return result;
        }
        public static void Logout()
        {
            RaiseLogoutHandler();
        }
        public static void RaiseLoginHandler()
        {
            LoginHandler?.Invoke(null, new EventArgs());
        }
        public static void RaiseLogoutHandler()
        {
            TOKEN = string.Empty;
            LogoutHandler?.Invoke(null, new EventArgs());
        }
        public static void RaiseRegisterHandler()
        {
            RegisterHandler?.Invoke(null, new EventArgs());
        }
        public static void Init()
        {
            token = Settings.Token;
        }
        public static bool IsTokenExists
        {
            get
            {
                if (token == null)
                {
                    token = string.Empty;
                }
                return (token == string.Empty);
            }
        }
        public static string TOKEN
        {
            get
            {
                return token;
            }
            set
            {
                Settings.Token = value;
                token = value;
            }
        }
        public static string ServerName
        {
            get
            {
                return SERVER_NAME;
            }
        }
        public static string LogIn_ApiUri
        {
            get
            {
                return SERVER_NAME + LOGIN_API;
            }
        }
        public static string Register_ApiUri
        {
            get
            {
                return SERVER_NAME + REGISTER_API;
            }
        }
        public static string PatientGet_ApiUri
        {
            get
            {
                return SERVER_NAME + PATIENT_GET_API;
            }
        }
        public static string PatientSet_ApiUri
        {
            get
            {
                return SERVER_NAME + PATIENT_SET_API;
            }
        }
        public static string TestHeader_ApiUri
        {
            get
            {
                return SERVER_NAME + TEST_HEADER_API;
            }
        }
        public static string Sms_ApiUri
        {
            get
            {
                return SERVER_NAME + SMS_API;
            }
        }
        public static string FileAll_ApiUri
        {
            get
            {
                return SERVER_NAME + FILE_ALL_API;
            }
        }
        public static string TherapyFileAll_ApiUri(string therapyId)
        {
            return SERVER_NAME + FILE_ALL_API + "?id_therapy=" + therapyId;
        }
        public static string GetAllTherapies_ApiUri
        {
            get
            {
                return SERVER_NAME + GET_ALL_THERAPIES;
            }
        }
        public static string CommentAllBySpecialist_ApiUri(string specialistId)
        {
            return SERVER_NAME + COMMENT_ALL_BY_SPECIALIST_API + "?id_specialist=" + specialistId;
        }
        public static string CommentAllClinic_ApiUri
        {
            get
            {
                return SERVER_NAME + COMMENT_ALL_CLINIC;
            }
        }
        public static string CommentAllClinicAnon_ApiUri
        {
            get
            {
                return SERVER_NAME + COMMENT_ALL_CLINIC_ANON;
            }
        }
        public static string Price_ApiUri
        {
            get
            {
                return SERVER_NAME + PRICE_API;
            }
        }
        public static string SpecialistAll_ApiUri
        {
            get
            {
                return SERVER_NAME + SPECIALIST_ALL_API;
            }
        }
        public static string Validation_ApiUri
        {
            get
            {
                return SERVER_NAME + VALIDATION_API;
            }
        }
        public static string CreateEntry_ApiUri(string specialistId, string date, string time, string note="")
        {
            return SERVER_NAME + CREATE_ENTRY + "?id_specialist=" + specialistId + "&time_from=" + date + " " + time + (note==""?"":("&note=" + note));
        }
        public static string CommentAdd_ApiUri(string specialistId, string text)
        {
            return SERVER_NAME + COMMENT_ADD_API + "?id_specialist=" + specialistId + "&text='" + text + "'"; 
        }
        public static string CommentClinicAdd_ApiUri(string text)
        {
            return SERVER_NAME + COMMENT_CLINIC_ADD_API + "?text='" + text + "'";
        }
        public static string phone = "";
        public static string Name = "";
        public static string back_call_add()
        {
            String s = Globals.PostRequest(Globals.PatientGet_ApiUri);
            JObject response = JObject.Parse(s);

            if (response.Value<string>("type") == "ok")
            {
                response = JObject.Parse(response.GetValue("response").ToString());
            }
            return SERVER_NAME + BACK_CALL_ADD + "?name=" + response.GetValue("firstname").ToString() + "&phone=" + response.GetValue("phone").ToString();
        }
        public static string GetAvailableTime_ApiUri(string specialistId, string date)
        {
            string uri = SERVER_NAME + GET_AVAILABLE_TIME_API + "?id_specialist=" + specialistId + "&day=" + date;
            Debug.Print(uri);
            return uri;
        }
        public static string GetTherapy_ApiUri(string therapyId)
        {
            return SERVER_NAME + GET_THERAPY + "&id_therapy=" + therapyId;
        }
        public static string GetEntry_ApiUri(string entryId)
        {
            return SERVER_NAME + GET_ENTRY + "&id_entry=" + entryId;
        }
        public static string GetAllEntries_ApiUri(string therapyId)
        {
            return SERVER_NAME + GET_ALL_ENTRIES + "&id_therapy=" + therapyId;
        }
    }
}
