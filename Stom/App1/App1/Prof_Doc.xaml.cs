﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Stomatology
{
    public partial class Prof_Doc : ContentPage
    {
        public class Document
        {

            public string name;
            public string date;
            public string link;
            public Image icon;
            public Label label0;
            public Label label1;
            public StackLayout stackLayoutH;
            public StackLayout stackLayoutD= new StackLayout();

            public Document()
            {
                name = "Test";//string.Empty;
                date = "2000-01-01";// string.Empty;
                link = "http://";
            }
            public Document(string name, string date, string link)
            {
                this.name = name;
                this.date = date;
                this.link = link;

                label0 = new Label()
                {
                    Text = this.name,
                    TextColor = Color.Black,
                    VerticalOptions = LayoutOptions.Center
                };

                label1 = new Label()
                {
                    Text = this.date,
                    TextColor = Color.Black,
                    HorizontalOptions = LayoutOptions.EndAndExpand,
                    VerticalOptions = LayoutOptions.Center,
                    TranslationX = -20
                };

                icon = new Image
                {
                    Source = "document.png",
                    Scale = 0.3f,
                    Margin = -20
                };

                stackLayoutH = new StackLayout
                {
                    GestureRecognizers = {
                        new TapGestureRecognizer
                        {
                            Command = new Command(() => OnClick())
                        }
                    },
                    Children =
                    {
                        icon,
                        label0,
                        label1
                    },
                    Orientation = StackOrientation.Horizontal,
                    BackgroundColor = Color.White,
                    Opacity = 0.7f                   
                 };
                stackLayoutD.Children.Add(stackLayoutH);              
            }

            public void OnClick()
            {
                Device.OpenUri(new Uri(link));
                //Globals.CurrentPage.DisplayAlert(name, link, "Ok!");
            }
        }

        public Prof_Doc()
        {
            Globals.CurrentPage = this;
            List<Document> docs = new List<Document>();
            string response = Globals.PostRequest(Globals.FileAll_ApiUri);
            JToken jsonResponse = JToken.Parse(response);
            if (jsonResponse[Globals.TYPE_KEY].ToString() == "ok")
            {
                JArray files = (JArray)jsonResponse[Globals.RESPONSE_KEY];
                foreach (JToken file in files)
                {
                    Document d = new Document(
                        file["name"].ToString(),
                        file["created_at"].ToString(),
                        file["link"].ToString()
                        );
                    docs.Add(d);
                }
            }
            GenerateDocLayout(docs);
            InitializeComponent();
        }
        public void GenerateDocLayout(List<Document> docs)
        {
            StackLayout s = new StackLayout() { Spacing = 3 };
            for (int i = 0; i < docs.Count; i++)
            {
                s.Children.Add(docs[i].stackLayoutD);
            } 
            Content = s;
        }
    }
}
