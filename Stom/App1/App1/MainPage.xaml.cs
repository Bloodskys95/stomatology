﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
namespace Stomatology
{
    public partial class MainPage : MasterDetailPage
    {
        public List<ItemsMenu> ListItemsMenu = new List<ItemsMenu>()
        {
            new ItemsMenu{TextItemsmenu="Запись на прием", ImageItemsMenu="addNewEntry.png" },
            new ItemsMenu{TextItemsmenu="Профиль", ImageItemsMenu="profile.png"  },
            new ItemsMenu{TextItemsmenu="Обратный звонок", ImageItemsMenu="myHistory.png"  },
            new ItemsMenu{TextItemsmenu="Прайс-лист", ImageItemsMenu="price.png"  },
            new ItemsMenu{TextItemsmenu="Специалисты", ImageItemsMenu="specialists.png"  },
            new ItemsMenu{TextItemsmenu="О клинике", ImageItemsMenu="infoAboutClinic.png"  },
            new ItemsMenu{TextItemsmenu="О разработчике", ImageItemsMenu="developers.png"  },
            new ItemsMenu{TextItemsmenu="Войти", ImageItemsMenu="enter.png"  }
        };

        public class ItemsMenu
        {
            public string TextItemsmenu { get; set; }
            public string ImageItemsMenu { get; set; }
        }


        public void OnRegister(object sender, EventArgs args)
        {
            this.Detail = new NavigationPage(new SmsConfirmationPage());
        }

        public void OnLogIn(object sender, EventArgs args)
        {
            ListItemsMenu[7].TextItemsmenu = "Выйти";
            ListItemsMenu[7].ImageItemsMenu = "exit.png";
            MessagingCenter.Send<MasterDetailPage>(this, "Refresh");
            this.Detail = new NavigationPage(new ProfMainPages());
        }

        public void OnLogOut(object sender, EventArgs args)
        {
            this.Detail = new NavigationPage(new InfoClinicPage());
        }

        public MainPage()
        {

            if (Globals.IsTokenExistsAndValid())
            {
                ListItemsMenu[7].TextItemsmenu = "Выйти";
                ListItemsMenu[7].ImageItemsMenu = "exit.png";
            }
            else
            {
                ListItemsMenu[7].TextItemsmenu = "Войти";
                ListItemsMenu[7].ImageItemsMenu = "enter.png";
            }


            Globals.LoginHandler += OnLogIn;
            Globals.RegisterHandler += OnRegister;
            Globals.LogoutHandler += OnLogOut;

            Label header = new Label
            {
                Text = "MasterDetailPage",
                FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
                HorizontalOptions = LayoutOptions.Center,
                Margin = 60,
                TextColor = Color.Black
            };
            Image Heder = new Image
            {
                Source= "photo1.jpg",
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Margin=0
            };


            // Create ListView for the master page.
            ListView listView = new ListView
            {
                AnchorY = 20,

                HasUnevenRows = true,
                ItemsSource = ListItemsMenu,
                ItemTemplate = new DataTemplate(() =>
                {
                    ImageCell imageCell = new ImageCell { TextColor = Color.Gray };

                    imageCell.SetBinding(TextCell.TextProperty, "TextItemsmenu");

                    imageCell.SetBinding(ImageCell.ImageSourceProperty, "ImageItemsMenu");

                    return imageCell;
                })
            };

            // Create the master page with the ListView.

            this.Master = new ContentPage
            {
                Title = header.Text,
                Content = new StackLayout
                {
                    Children = { Heder, listView }
                }
            };

            MessagingCenter.Subscribe<MasterDetailPage>(this, "Refresh", (s) =>
            {
                listView.ItemsSource = null;
                listView.ItemsSource = ListItemsMenu;
            });
            // Create the detail page using NamedColorPage and wrap it in a
            // navigation page to provide a NavigationBar and Toggle button

            this.Detail = new NavigationPage(new SpecList());

            // Define a selected handler for the ListView.
            listView.ItemTapped += ListView_ItemTapped;
        }
        public void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {

            ItemsMenu selectedItemsMenu = e.Item as ItemsMenu;

            Debug.WriteLine(e);
            switch (selectedItemsMenu.TextItemsmenu)
            {
                case "Запись на прием":
                    if (!Globals.IsTokenExistsAndValid())
                    {
                        this.Detail = new NavigationPage(new AuthPage());
                    }
                    else
                    {
                        this.Detail = new NavigationPage(new EntryCreatingPage());
                    }
                    IsPresented = false;
                    break;

                case "Профиль":
                    if (!Globals.IsTokenExistsAndValid())
                    {
                        this.Detail = new NavigationPage(new AuthPage());
                    }
                    else
                    {
                        this.Detail = new NavigationPage(new ProfMainPages());
                    }
                    IsPresented = false;
                    break;

                case "Обратный звонок":
                    if (!Globals.IsTokenExistsAndValid())
                    {
                        this.Detail = new NavigationPage(new AuthPage());
                    }
                    else
                    {
                        Globals.PostRequest(Globals.back_call_add()); 
                    }
                    IsPresented = false;
                    break;

                case "Прайс-лист":
                    this.Detail = new NavigationPage(new PricePage());
                    IsPresented = false;
                    break;

                case "Специалисты":
                    this.Detail = new NavigationPage(new SpecList());
                    IsPresented = false;
                    break;

                case "О клинике":
                    this.Detail = new NavigationPage(new InfoClinicPage());
                    IsPresented = false;
                    break;

                case "О разработчике":

                    Device.OpenUri(new Uri("http://app-labs.ru/"));
                    IsPresented = false;
                    break;

                case "Выйти":
                    ListItemsMenu[7].TextItemsmenu = "Войти";
                    ListItemsMenu[7].ImageItemsMenu = "enter.png";
                    MessagingCenter.Send<MasterDetailPage>(this, "Refresh");
                    Globals.Logout();
                    IsPresented = false;
                    break;
                case "Войти":
                    this.Detail = new NavigationPage(new AuthPage());
                    IsPresented = false;
                    break;
            }
            // Set the BindingContext of the detail page.
            Detail.BindingContext = e.Item;
            //header.Text = args.SelectedItem.ToString();

            // Show the detail page.
            IsPresented = false;

        }
    }
}

