﻿
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Newtonsoft.Json.Linq;

namespace Stomatology
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ProfUser : ContentPage
	{
        Entry NameEntry = new Entry
        {
            Text = "Мужской",
            FontSize = 20,
            BackgroundColor = Color.White,
            TextColor = Color.FromHex("3e3e3e"),
            IsEnabled =false
        };
        Entry SurnameEntry = new Entry
        {
            Text = "Мужской",
            FontSize = 20,
            BackgroundColor = Color.White,
            TextColor = Color.FromHex("3e3e3e"),
            IsEnabled = false
        };
        Entry PatronymicEntry = new Entry
        {
            Text = "Мужской",
            FontSize = 20,
            BackgroundColor = Color.White,
            TextColor = Color.FromHex("3e3e3e"),
            IsEnabled = false
        };
        DatePicker DataEntry = new DatePicker
        {
            BackgroundColor = Color.White,
            TextColor = Color.FromHex("3e3e3e"),
            IsEnabled = false
        };
        StackLayout EntryStack = new StackLayout
        {
            Orientation = StackOrientation.Vertical,
            IsVisible = true
        };

        Button male = new Button
        {
            Text ="Мужской",
            BackgroundColor = Color.White,
            TextColor = Color.FromHex("3e3e3e"),
            HorizontalOptions = LayoutOptions.FillAndExpand,
            IsEnabled = false
        };
        Button female = new Button
        {
            Text = "Женский",
            BackgroundColor = Color.White,
            TextColor = Color.FromHex("3e3e3e"),
            HorizontalOptions = LayoutOptions.FillAndExpand,
            IsEnabled = false
        };
        StackLayout Pol = new StackLayout
        {
            Orientation =StackOrientation.Horizontal            
        };

        Button edit = new Button
        {
            Text = "Редактировать",
            BackgroundColor = Color.White,
            TextColor = Color.FromHex("3e3e3e"),
            TranslationY = 10
        };

        StackLayout mainStack = new StackLayout
        {
            Orientation = StackOrientation.Vertical,
            Padding = 30
        };

        bool Ed = false;

        public ProfUser()
        {
            GetData();
            InitializeComponent();

            EntryStack.Children.Add(NameEntry);
            EntryStack.Children.Add(SurnameEntry);
            EntryStack.Children.Add(PatronymicEntry);
            EntryStack.Children.Add(DataEntry);

            Pol.Children.Add(male);
            Pol.Children.Add(female);

            mainStack.Children.Add(EntryStack);
            mainStack.Children.Add(Pol);
            mainStack.Children.Add(edit);

            edit.Clicked += Edit_Clicked;
            male.Clicked += Male_Clicked;
            female.Clicked += Female_Clicked;

            Content = mainStack;
        }

        private void Female_Clicked(object sender, EventArgs e)
        {
            female.BorderColor = Color.FromHex("b685ce");
            female.BorderWidth = 3;
            male.BorderWidth = 0;
        }

        private void Male_Clicked(object sender, EventArgs e)
        {
            male.BorderColor = Color.FromHex("b685ce");
            male.BorderWidth = 3;
            female.BorderWidth = 0;
        }

        public void Edit_Clicked(object sender, EventArgs e)
        {
            if (!Ed)
            {
                NameEntry.IsEnabled = true;
                SurnameEntry.IsEnabled = true;
                PatronymicEntry.IsEnabled = true;
                DataEntry.IsEnabled = true;
                male.IsEnabled = true;
                female.IsEnabled = true;
                Ed = !Ed;
            }
            else
            {
                var answer = DisplayAlert("Внимание!", "Данные сохранены!", "Ok");
                Globals.Name = SurnameEntry.Text;
                NameValueCollection data = new NameValueCollection();
                data.Add("surname", SurnameEntry.Text);
                data.Add("firstname", NameEntry.Text);
                data.Add("patronymic", PatronymicEntry.Text);
                data.Add("sex", GetSex());
                data.Add("birthday", SetBirth());

                Globals.PostRequest(Globals.PatientSet_ApiUri, data);
                NameEntry.IsEnabled = false;
                SurnameEntry.IsEnabled = false;
                PatronymicEntry.IsEnabled = false;
                DataEntry.IsEnabled = false;
                male.IsEnabled = false;
                female.IsEnabled = false;
                Ed = !Ed;
            }
        }

        public void GetData()
        {
            String s = Globals.PostRequest(Globals.PatientGet_ApiUri);
            JObject response = JObject.Parse(s);

            if (response.Value<string>("type") == "ok")
            {
                response = JObject.Parse(response.GetValue("response").ToString());
            }

            NameEntry.Text = response.GetValue("firstname").ToString();
            SurnameEntry.Text = response.GetValue("surname").ToString();
            PatronymicEntry.Text = response.GetValue("patronymic").ToString();
            if (response.GetValue("sex").ToString() == "M")
            {
                male.BorderColor = Color.FromHex("b685ce");
                male.BorderWidth = 3;
                female.BorderWidth = 0;
            }
            else
            {
                female.BorderColor = Color.FromHex("b685ce");
                female.BorderWidth = 3;
                male.BorderWidth = 0;
            }
            //Проверка на существование даты в поле дата
            if (response.GetValue("birthday").ToString() == DBNull.Value.ToString()) return;
            DataEntry.Date = Convert.ToDateTime(response.GetValue("birthday").ToString());
        }

        private string GetSex()
        {
            if (female.BorderWidth == 0)
            {
                return "M";
            }
            else
            {            
                return "F";
            }
        }

        private string SetBirth()
        {
            return DataEntry.Date.ToString("yyyy-MM-dd");
        }
    }
}
