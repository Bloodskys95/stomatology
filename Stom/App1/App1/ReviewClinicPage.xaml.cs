﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Stomatology
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ReviewClinicPage : ContentPage
    {
        
        StackLayout StackReviews = new StackLayout();
        //Поле ввода коментария
        Entry ReviwEntru = new Entry()
        {
            TextColor = Color.Gray,
            Placeholder = "Ваш коментарий",
            IsVisible = false,
            BackgroundColor = Color.White
        };
        //Кнопка отправки на сервер
        Button Ok = new Button()
        {
            Text = "Отправить",
            TextColor = Color.Gray,
            IsVisible = false,
            BackgroundColor = Color.White
        };

        public ReviewClinicPage()
        {
            InitializeComponent();
            foreach(var comment in GetAllClinicComments())
            {
                Reviews(comment.initials, "", comment.date, comment.text);
            }
            //Изображене клиники
            Image Avatar = new Image
            {
                Source = "photo1.jpg",
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };
            //Кнопка открытия формы коментария
            Button Review = new Button()
            {
                Text = "Оставить отзыв",
                TextColor = Color.Gray,
                BackgroundColor = Color.White
            };        
            Review.Clicked += Review_Clicked;

            Ok.Clicked += Ok_Clicked;
            //добавление верхней части страници
            StackLayout StackProfil = new StackLayout()
            {
                Children = { Avatar, Review, ReviwEntru, Ok }
            };
            //добавление прокруткик коментариев
            Content = new ScrollView { Content = StackReviews };
            //объединение
            StackLayout MainStack = new StackLayout()
            {
                Children = { StackProfil, Content }
            };

            Content = MainStack;
        }
        private List<Comment> GetAllClinicComments()
        {
            string response = Globals.PostRequest(Globals.CommentAllClinic_ApiUri);
            JToken jsonResponse = JToken.Parse(response);
            List<Comment> comments = new List<Comment>();
            if (jsonResponse[Globals.TYPE_KEY].ToString() == "ok")
            {
                JArray files = (JArray)jsonResponse[Globals.RESPONSE_KEY];
                foreach (JToken file in files)
                {
                    Comment comment = new Comment(
                        file["id"].ToString(),
                        file["id_user"].ToString(), "",
                        file["text"].ToString(),
                        file["updated_at"].ToString(), 
                        file["patient"]["surname"] + " " + file["patient"]["firstname"].ToString()[0] + ". " + file["patient"]["patronymic"].ToString()[0] + ".");
                    comments.Add(comment);
                }
            }
            return comments;
        }
        private string AddClinicComment(string text)
        {
            return Globals.PostRequest(Globals.CommentClinicAdd_ApiUri(text));
        }
        //Отправка комента на сервер, и  надо добавить(обновление страници коментариев)
        private void Ok_Clicked(object sender, EventArgs e)
        {
            ReviwEntru.IsVisible = false;
            Ok.IsVisible = false;
            string response = AddClinicComment(ReviwEntru.Text);
            JToken jsonResponse = JToken.Parse(response);

            if (jsonResponse[Globals.TYPE_KEY].ToString() == "ok")
            {
                JToken patient = jsonResponse[Globals.RESPONSE_KEY];
                string initials =
                    patient["patient"]["surname"] +
                    " " +
                    patient["patient"]["firstname"].ToString()[0] +
                    ". " +
                    patient["patient"]["patronymic"].ToString()[0] +
                    ".";
                Reviews(initials, "", patient["updated_at"].ToString(), patient["text"].ToString());
            }
            ReviwEntru.Text = string.Empty;
        }

        //Открытие формы добавления комента
        private void Review_Clicked(object sender, EventArgs e)
        {
            if (!Globals.IsTokenExistsAndValid())
            {
                DisplayAlert("Ошибка!","Авторизируйтесь для того чтобы оставить коментарий","Ok");
                return;
            }
                ReviwEntru.IsVisible = true;
                Ok.IsVisible = true;
        }


        //Функци добавления коментариев
        public void Reviews(string titleReview,string fIOReviews,string dataReviews,string reviewsText)
        {
            Label TitleReview = new Label()
            {
                Text = titleReview/*"Заголовок отзыва"*/,
                TextColor = Color.Gray
            };

            Label FIOReviews = new Label()
            {
                Text = fIOReviews /*"Иванов И. И."*/,
                TextColor = Color.Gray
            };

            Label DataReviews = new Label()
            {
                Text = dataReviews /*"17 марта 2017"*/,
                TextColor = Color.Gray
            };

            Label ReviewsText = new Label()
            {
                Text = reviewsText/*"bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla"*/,
                TextColor = Color.Gray
            };

            StackLayout StackFIOData = new StackLayout()
            {
                Children = { FIOReviews, DataReviews }
            };
            StackFIOData.Orientation = StackOrientation.Horizontal;

            StackLayout StackReview = new StackLayout()
            {
                Children = { TitleReview, StackFIOData, ReviewsText }
            };
            StackReview.BackgroundColor = Color.White;
            StackReview.Padding = 20;

            StackReviews.Children.Insert(0, StackReview);
        }

    }
}
