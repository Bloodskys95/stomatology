﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Stomatology
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Appointments : ContentPage
	{
            StackLayout MainTreatment = new StackLayout()
            {
                BackgroundColor =Color.White
            };
		public Appointments (string entryId)
		{
			InitializeComponent ();
            NewAppointments(
                "Вид лечения",
                "Название лечения",
                "Стоимость лечения",
                "ДД.ММ.ГГГГ",
                "ДД.ММ.ГГГГ",
                "Имя лечащего врача",
                4,
                "Документ1", 
                "Рекомендации");
            Content = new ScrollView { Content = MainTreatment };
        }

        public void NewAppointments(
            string typeOfTreat,
            string nameOfTreat,
            string costOfTreat,
            string dateOfConsul,
            string dateOfTreat,
            string therapistFIO,
            int DocCount,
            string docs,
            string recom)
        {
            
            Label TypeOfTreatment = new Label() //Вид лечения
            {
                Text = typeOfTreat,
                FontSize = 20,
                TextColor = Color.FromHex("b685ce"),
                Opacity = 0.6
            };
            Label NameOfTreatment = new Label()//Название лечения
            {
                Text = nameOfTreat,
                FontSize = 22,
                TextColor = Color.FromHex("757575"),                
                Opacity = 0.6
            };
            Label CostOfTreatment = new Label() //Стоимость лечения
            {
                Text = costOfTreat,
                FontSize = 24,
                TextColor = Color.FromHex("3e3e3e"),
                Opacity = 0.8
            };
            StackLayout Treat = new StackLayout()
            {
                Spacing=3,
                Children =
                {
                    TypeOfTreatment ,
                    NameOfTreatment ,
                    CostOfTreatment
                }
            };

            //Дата предварительной консультации (Елси есть)
            Image calendar1 = new Image()
            {
                Margin=-30,
                Scale = 0.3,
                Source = "calendar.png"
            };
            Image calendar2 = new Image()
            {
                Margin = -30,
                Scale = 0.3,
                Source = "calendar.png"
            };
            Label LabelDateOfConsultation = new Label()
            {
                Text = "Дата предварительной консультации",
                IsVisible =true,
                FontSize = 20,
                TextColor = Color.FromHex("b685ce"),
                Opacity = 0.6
            };
            Label DateOfConsultation = new Label()
            {
                VerticalOptions = LayoutOptions.Center,
                Text = dateOfConsul,
                IsVisible = true,
                FontSize = 18,
                TextColor = Color.FromHex("757575"),
                Opacity = 0.6
            };
            StackLayout DataCall = new StackLayout()
            {
                VerticalOptions = LayoutOptions.Center,
                Orientation =StackOrientation.Horizontal,
                Children = { calendar1, DateOfConsultation }
            };

            //Дата лечения
            Label LabelDateOfTreatment = new Label()
            {
                Text = "Дата лечения",
                FontSize = 20,
                TextColor = Color.FromHex("b685ce"),
                Opacity = 0.6
            };
            Label DateOfTreatment = new Label() //Открытие календаря. синхронизация с календарем
            {
                VerticalOptions = LayoutOptions.Center,
                Text = dateOfTreat,
                FontSize = 18,
                TextColor = Color.FromHex("757575"),
                Opacity = 0.6
            };
            StackLayout DataCall2 = new StackLayout()
            {
                
                Orientation = StackOrientation.Horizontal,
                Children = { calendar2, DateOfTreatment }
            };
            StackLayout Date = new StackLayout()
            {
                Children =
                {
                    LabelDateOfConsultation ,
                     DataCall,
                    LabelDateOfTreatment ,
                    DataCall2
                }
            };

            //Имя лечущего врача
            Label LabelTherapist = new Label()
            {
                Text = "Имя лечащего врача",
                FontSize = 20,
                TextColor = Color.FromHex("b685ce"),
                Opacity = 0.6
            };
            Label TherapistFIO = new Label() //ссылка на страницу врача с коментариями
            {
                GestureRecognizers =
                {
                    new TapGestureRecognizer
                    {
                        Command = new Command(() => OnSpecClick())
                    },
                },
                Text = therapistFIO,
                FontSize = 18,
                TextColor = Color.FromHex("757575"),
                Opacity = 0.6
            };
            StackLayout Therapist = new StackLayout()
            {
                Children =
                {
                    LabelTherapist,
                    TherapistFIO
                }
            };
            
 
            //Прикрепленные документы

            Label LabelDoc = new Label()
            {
                Text = "Прикрепленные документы",
                FontSize = 20,
                TextColor = Color.FromHex("b685ce"),
                Opacity = 0.6
            };
            StackLayout Doc = new StackLayout()
            {
                Children = { LabelDoc }
            };
            for (int i = 0; i < DocCount; i++)
            {
                Image DocImag = new Image()
                {
                    Margin = -30,
                    Scale = 0.3,
                    Source = "clip.png"
                };
                Label Docs = new Label()
                {
                    Text = docs,
                    FontSize = 18,
                    TextColor = Color.FromHex("757575"),
                    Opacity = 0.6
                };
                StackLayout DocIm = new StackLayout()
                {
                    VerticalOptions=LayoutOptions.Center,
                    Orientation =StackOrientation.Horizontal,
                    Children =
                    {
                       DocImag , Docs
                    }
                };
                Doc.Children.Add(DocIm);
            }

            //Рекомендации врача
            Label LabelRecom = new Label()
            {
                Text = "Рекомендации врача",
                FontSize = 20,
                TextColor = Color.FromHex("b685ce"),
                Opacity = 0.6
            };
            Label Recom = new Label()
            {
                Text = recom,
                FontSize = 18,
                TextColor = Color.FromHex("757575"),
                Opacity = 0.6
            };
            StackLayout Rec = new StackLayout()
            {
                Children =
                {
                    LabelRecom,Recom
                }
            };

            StackLayout Treatment = new StackLayout()
            {
                Children =
                {
                    Treat,
                    Date,
                    Therapist,
                    Doc,
                    Rec
                },
                Margin=20,
                Spacing=20,
                Orientation=StackOrientation.Vertical              
            };
            MainTreatment.Children.Add(Treatment);
        }
        public async void OnSpecClick()
        {
            await Navigation.PushAsync(new SpecList());
        }
    }
}
