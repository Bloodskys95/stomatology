﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Stomatology
{
    public class Specialist
    {
        //TODO: Add rest fields if needed
        public string id;
        public string surname;
        public string firstname;
        public string patronymic;
        public string sex;
        public string avatar;
        public string note;
        public string job;

        public Specialist(
            string id,
            string surname,
            string firstname,
            string patronymic,
            string avatar,
            string sex,
            string note)
        {
            this.id = id;
            this.surname = surname;
            this.firstname = firstname;
            this.patronymic = patronymic;
            this.avatar = avatar;
            this.sex = (sex == "M" ? "Мужской" : "Женский");
            this.note = (note == "null" ? "" : note);
            //TODO: Add job column to DB
            job = "Профессия";
        }
    }
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SpecList : ContentPage
	{
        static Color DefaultTextColor = Color.Gray;
        static string DefaultAvatarImage = "photoPlace2.png";
        static string DefaultArrowImage = "arrowRight.png";
        StackLayout main = new StackLayout() { Spacing = 4 };
		public SpecList ()
		{
            InitializeComponent ();

            //Создание элементов Layout'а
            foreach (var specialist in GetAllScpecialists())
            {
                AddSpecialistToLayout(specialist);
            }

            Content = new ScrollView { Content = main };
        }

        //Создает на Layout элемент с данными специалиста
        private void AddSpecialistToLayout(Specialist specialist)
        {
            main.Children.Add(CreateWorker(
                specialist.id,
                specialist.firstname,
                specialist.surname,
                specialist.avatar,
                specialist.patronymic,
                specialist.job,
                specialist.note));
        }

        private StackLayout CreateWorker(
            string id,
            string firstname,
            string surname,
            string avatar,
            string patronymic,
            string job,
            string description)
        {
            Image profilePicture = new Image()
            {
                Source = avatar, //DefaultAvatarImage,
                HorizontalOptions = LayoutOptions.Start,
                TranslationX=-20,
                Scale = 0.7
            };
            Label firsnameLabel = new Label()
            {
                Text = firstname,
                TextColor = DefaultTextColor
            };
            Label surnameLabel = new Label()
            {
                Text = surname,
                TextColor = DefaultTextColor
            };
            Label patronymicLabel = new Label()
            {
                Text = patronymic,
                TextColor = DefaultTextColor
            };
            Label jobLabel = new Label()
            {               
                Text = job,
                TextColor = Color.Black
            };

            Image arrowImage = new Image()
            {
                Source = DefaultArrowImage,
                HorizontalOptions = LayoutOptions.EndAndExpand,
                Scale = 0.3,
                Margin=0
            };

            StackLayout workerStack = new StackLayout()
            {
                Spacing = 0,
                Children = { surnameLabel,  firsnameLabel, patronymicLabel, jobLabel},
                VerticalOptions=LayoutOptions.CenterAndExpand,
                Margin=-10,
                Orientation = StackOrientation.Vertical
            };

            StackLayout tappableStack = new StackLayout()
            {
                
                GestureRecognizers =
                {
                    new TapGestureRecognizer
                    {
                        Command = new Command(() => OnSpecClick(
                            id,
                            firsnameLabel.Text,
                            surnameLabel.Text,
                            patronymicLabel.Text,
                            avatar,
                            jobLabel.Text)),
                    },
                },
                Children = { profilePicture, workerStack, arrowImage },
                BackgroundColor = Color.White,
                Spacing=1,
                
                Orientation = StackOrientation.Horizontal
            };

            return tappableStack;
        }

        // Открывает Layout "Профиль Специалиста" (как я понял)
        public async void OnSpecClick(String id, String firstname, String surname, String patronymic, String avatar, String job)
        {
            await Navigation.PushAsync(new Spec(id, firstname, surname, patronymic, avatar, job));
        }

        public List<Specialist> GetAllScpecialists()
        {
            string response = Globals.PostRequest(Globals.SpecialistAll_ApiUri);
            JToken jsonResponse = JToken.Parse(response);
            List<Specialist> specialist = new List<Specialist>();
            if (jsonResponse[Globals.TYPE_KEY].ToString() == "ok")
            {
                JArray files = (JArray)jsonResponse[Globals.RESPONSE_KEY];
                foreach (JToken file in files)
                {
                    Specialist s = new Specialist(
                        file["id"].ToString(),
                        file["surname"].ToString(),
                        file["firstname"].ToString(),
                        file["patronymic"].ToString(),
                        file["avatar"].ToString(),
                        file["sex"].ToString(),
                        file["note"].ToString());
                    specialist.Add(s);
                }
            }
            return specialist;
        }
    }   
}
