﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Stomatology
{
    public partial class Prof_Hist : ContentPage
    {
        StackLayout mainStack = new StackLayout() { Spacing = 3 };
        public Prof_Hist()
        {
            InitializeComponent();
            var t = TherapyLoader.GetAllTherapies();
            foreach(var a in t)
            {
                Debug.Print(a.specialist);
            }
            AddPreview("123", 2);
            AddPreview("123", 3);
            AddPreview("123", 4);
            Content = new ScrollView { Content = mainStack };
        }
        //Превью записи или группы лечения
        public void AddPreview(string Id_Zapis, int Count_Zapis)
        {
            Image clock = new Image()
            {
                Margin = -20,
                Scale = 0.3,
                Source = "clock.png"
            };

            Label Vid = new Label()
            {
                FontSize = 20,
                Text = "Вид лечения",
                TextColor = Color.Black
            };
            Label Kat = new Label()
            {
                FontSize = 18,
                Text = "Категория",
                TextColor = Color.Black
            };
            Label FIO = new Label()
            {
                FontSize = 20,
                Text = "ФИО Врача",
                TextColor = Color.Black
            };
            Label Data = new Label()
            {
                HorizontalOptions = LayoutOptions.EndAndExpand,
                Text = "17:03 12/03/2017",
                TextColor = Color.Black,
                TranslationX = -15,
                VerticalOptions = LayoutOptions.Center
            };
            StackLayout Zap = new StackLayout()
            {
                HorizontalOptions = LayoutOptions.Center,
                Orientation = StackOrientation.Vertical,
                VerticalOptions = LayoutOptions.Center,
                Children =
                {
                    Vid,
                    Kat ,
                    FIO
                }
            };

            StackLayout EntryStack = new StackLayout() { Spacing = 0, IsVisible = false, Opacity = 0.5 };

            StackLayout Preview = new StackLayout()
            {
                BackgroundColor = Color.White,
                Padding = 10,
                Orientation = StackOrientation.Horizontal,
                Children =
                {
                    clock,
                    Zap,
                    Data
                },
                GestureRecognizers = {
                        new TapGestureRecognizer
                        {
                            Command = new Command(() => OnClick_OpenPreview(EntryStack))
                        }
                    },
            };

            mainStack.Children.Add(Preview);

            int count_zapis = Count_Zapis;



            for (int i = 0; i < count_zapis; i++)
            {
                string id_zapis = Id_Zapis;
                EntryStack.Children.Add(AddZapis(id_zapis));
            }
            mainStack.Children.Add(EntryStack);
        }

        //Записи
        public StackLayout AddZapis(string IdZapis)
        {
            string id_zapis = IdZapis;
            Image clock = new Image()
            {
                Margin = -20,
                Scale = 0.3,
                Source = "clock.png"
            };

            Label Vid = new Label()
            {
                FontSize = 20,
                Text = "Вид лечения",
                TextColor = Color.Black
            };
            Label Kat = new Label()
            {
                FontSize = 18,
                Text = "Категория",
                TextColor = Color.Black
            };
            Label FIO = new Label()
            {
                FontSize = 20,
                Text = "ФИО Врача",
                TextColor = Color.Black
            };
            Label Data = new Label()
            {
                HorizontalOptions = LayoutOptions.EndAndExpand,
                Text = "17:03 12/03/2017",
                TextColor = Color.Black,
                TranslationX = -15,
                VerticalOptions = LayoutOptions.Center
            };
            StackLayout Zap = new StackLayout()
            {
                HorizontalOptions = LayoutOptions.Center,
                Orientation = StackOrientation.Vertical,
                VerticalOptions = LayoutOptions.Center,
                Children =
                {
                    Vid,
                    Kat ,
                    FIO
                }
            };
            StackLayout Zapis = new StackLayout()
            {
                BackgroundColor = Color.White,
                Padding = 10,
                Orientation = StackOrientation.Horizontal,
                Children =
                {
                    clock,
                    Zap,
                    Data
                },
                GestureRecognizers = {
                        new TapGestureRecognizer
                        {
                            Command = new Command(() => OnClick(IdZapis))
                        }
                    },

            };
            return Zapis;
        }
        public async void OnClick(string entryId)
        {
            await Navigation.PushAsync(new Appointments(entryId));
        }

        public void OnClick_OpenPreview(StackLayout EntryStack)
        {
            EntryStack.IsVisible = !EntryStack.IsVisible;
        }
    }
}
