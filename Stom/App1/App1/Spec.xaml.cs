﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Stomatology
{ 
    public class Comment
    {
        public string commentId;
        public string userId;
        public string specialistId;
        public string text;
        public string date;
        public string initials;
        public Comment(
            string commentId,
            string userId,
            string specialistId,
            string text,
            string date,
            string initials)
        {
            this.commentId = commentId;
            this.userId = userId;
            this.specialistId = specialistId;
            this.text = text;
            this.date = date;
            this.initials = initials;
        }
    }
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Spec : ContentPage
	{
        string specialistId;
        StackLayout StackReviews = new StackLayout();
        
        //Поле ввода коментария
        Entry ReviwEntru = new Entry()
        {
            TextColor = Color.Gray,
            Placeholder = "Ваш коментарий",
            IsVisible = false,
            BackgroundColor = Color.White
        };
        //Кнопка отправки на сервер
        Button Ok = new Button()
        {
            Text = "Отправить",
            TextColor = Color.Gray,
            IsVisible = false,
            BackgroundColor = Color.White
        };        
        //Тут все данные берутся из другой формы, так что нужно только коменты подключить =)
        public Spec (String id, String firstname, String surname, String patronymic, String avatar, String job)
		{
            InitializeComponent ();
            specialistId = id;
            foreach(var comment in AllComments(id))
            {
                //Ставлю инициалы на место "Темы"
                //TODO: Определиться, нужна ли тема. Если нет - можно оставить так.
                Reviews(comment.initials, "", comment.date, comment.text);
            }

            //Аватар
            Image Avatar = new Image
            {
                Source = avatar, //"photoPlace.png",
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                Scale = 0.6
            };
            //фамиля
            Label name1 = new Label()
            {
                Text =firstname,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                FontSize = 18,
                TextColor = Color.Black
            };
            //Имя
            Label name2 = new Label()
            {
                Text = surname,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                FontSize = 18,
                TextColor = Color.Black
            };
            //отчество
            Label name3 = new Label()
            {
                Text = patronymic,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                FontSize = 18,
                TextColor = Color.Black
            };
            //Стак имени и отчества
            StackLayout Name = new StackLayout() { Children = { name2,name3 } };
            Name.Orientation = StackOrientation.Horizontal;
            Name.HorizontalOptions = LayoutOptions.CenterAndExpand;
            //Специализация спеиалиста
            Label proff = new Label()
            {
                Text = job,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };
            //Кнопка открытия формы добавления комента
            Button Review = new Button()
            {
                Text ="Оставить отзыв",
                TextColor = Color.Gray,
                BackgroundColor = Color.White
            };
            Review.Clicked += Review_Clicked1;

            Ok.Clicked += Ok_Clicked1;
            //главный стак, с данными
            StackLayout StackProfil = new StackLayout()
            {
                Children = { Avatar, name1 , Name , proff, Review , ReviwEntru, Ok },
                Spacing=0
            };

            //Давление прокрутки коментариям
            StackLayout ScrollViewContent = new StackLayout { Children = { StackReviews }  };
            //объединение всех стаков
            StackLayout MainStack = new StackLayout()
            {
                Children = { StackProfil, ScrollViewContent }
            };

            Content =new ScrollView { Content = MainStack };

        }

        //Отправка комента на сервер, и  надо добавить(обновление страницы коментариев)
        private void Ok_Clicked1(object sender, EventArgs e)
        {   
            ReviwEntru.IsVisible = false;
            Ok.IsVisible = false;
            string response = AddComment(specialistId, ReviwEntru.Text);
            JToken jsonResponse = JToken.Parse(response);
            
            if (jsonResponse[Globals.TYPE_KEY].ToString() == "ok")
            {
                JToken patient = jsonResponse[Globals.RESPONSE_KEY];
                string initials = 
                    patient["patient"]["surname"] + 
                    " " + 
                    patient["patient"]["firstname"].ToString()[0] + 
                    ". " + 
                    patient["patient"]["patronymic"].ToString()[0] + 
                    ".";
                Reviews(initials, "", patient["updated_at"].ToString(), patient["text"].ToString());
            }
            ReviwEntru.Text = string.Empty;
        }

        //Отправка комментария
        private string AddComment(string specialistId, string commentText)
        {
            return Globals.PostRequest(Globals.CommentAdd_ApiUri(specialistId, commentText));
        }

        private List<Comment> AllComments(String specialistId)
        {
            string response = Globals.PostRequest(Globals.CommentAllBySpecialist_ApiUri(specialistId));
            JToken jsonResponse = JToken.Parse(response);
            List<Comment> comments = new List<Comment>();
            if (jsonResponse[Globals.TYPE_KEY].ToString() == "ok")
            {
                JArray files = (JArray)jsonResponse[Globals.RESPONSE_KEY];
                foreach (JToken file in files)
                {
                    Comment comment = new Comment(
                        file["id"].ToString(),
                        file["id_user"].ToString(),
                        file["id_specialist"].ToString(),
                        file["text"].ToString(),
                        file["updated_at"].ToString(),
                        file["patient"]["surname"] + " " + file["patient"]["firstname"].ToString()[0] + ". " + file["patient"]["patronymic"].ToString()[0] + ".");
                    comments.Add(comment);
                }
            }
            return comments;
        }
        //Открытие формы добавления комента
        private void Review_Clicked1(object sender, EventArgs e)
        {
            if (!Globals.IsTokenExistsAndValid())
            {
                DisplayAlert("Ошибка!", "Авторизируйтесь для того чтобы оставить коментарий", "Ok");
                return;
            }
            ReviwEntru.IsVisible = true;
            Ok.IsVisible = true;
        }
        //Функци добавления коментариев
        public void Reviews(string titleReview, string fIOReviews, string dataReviews, string reviewsText)
        {
            Label TitleReview = new Label()
            {
                Text = titleReview/*"Заголовок отзыва"*/,
                TextColor = Color.Gray
            };

            Label FIOReviews = new Label()
            {
                Text = fIOReviews /*"Иванов И. И."*/,
                TextColor = Color.Gray
            };

            Label DataReviews = new Label()
            {
                Text = dataReviews /*"17 марта 2017"*/,
                TextColor = Color.Gray
            };

            Label ReviewsText = new Label()
            {
                Text = reviewsText/*"bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla"*/,
                TextColor = Color.Gray
            };

            StackLayout StackFIOData = new StackLayout()
            {
                Children = { FIOReviews, DataReviews }
            };
            StackFIOData.Orientation = StackOrientation.Horizontal;

            StackLayout StackReview = new StackLayout()
            {
                Children = { TitleReview, StackFIOData, ReviewsText }
            };
            StackReview.BackgroundColor = Color.White;
            StackReview.Padding = 20;

            StackReviews.Children.Insert(0,StackReview);
        }
    }
}
