﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;

namespace Stomatology
{
    public class TherapyLoader
    {
        public static List<Therapy> GetAllTherapies()
        {
            List<Therapy> result = new List<Therapy>();
            string response = Globals.PostRequest(Globals.GetAllTherapies_ApiUri);
            JToken jsonResponse = JToken.Parse(response);
            if (jsonResponse[Globals.TYPE_KEY].ToString() == "ok")
            {
                JArray therapyArray = JArray.Parse(jsonResponse[Globals.RESPONSE_KEY].ToString());
                foreach (JToken therapy in therapyArray)
                {
                    result.Add(new Therapy(therapy["id"].ToString()));
                }
            }
            return result;
        }
        public static Therapy GetTherapy(string therapyId)
        {
            return new Therapy(therapyId);
        }
        public static TherapyEntry GetEntry(string therapyId, string entryId)
        {
            return new TherapyEntry(therapyId, entryId);
        }
        /*
        public static List<TherapyEntryShort> GetAllEntries(string therapyId)
        {
            List<TherapyEntry> result = new List<TherapyEntry>();
            string response = Globals.PostRequest(Globals.GetAllEntries_ApiUri(therapyId));
            JToken jsonResponse = JToken.Parse(response);
            if (jsonResponse[Globals.TYPE_KEY].ToString() == "ok")
            {
                JArray therapyArray = JArray.Parse(jsonResponse[Globals.RESPONSE_KEY].ToString());
                foreach (JToken therapy in therapyArray)
                {
                    Therapy t = new Therapy();
                    t.id = therapy["id"].ToString();
                    t.id_user = therapy["id_user"].ToString();
                    t.name = therapy["name"].ToString();
                    t.note = therapy["note"].ToString();
                    t.created_at = therapy["created_at"].ToString();
                    t.updated_at = therapy["updated_at"].ToString();
                    t.recommendation = therapy["recommendation"].ToString();
                }
            }
            return result;
        }
        public static TherapyEntryExtended GetEntry(string entryId)
        {
            TherapyEntryExtended result = new TherapyEntryExtended();
            string response = Globals.PostRequest(Globals.GetEntry_ApiUri(entryId));
            JToken jsonResponse = JToken.Parse(response);
            if (jsonResponse[Globals.TYPE_KEY].ToString() == "ok")
            {
                JToken therapy = JToken.Parse(jsonResponse[Globals.RESPONSE_KEY].ToString());
                Therapy t = new Therapy()
                {
                    id = therapy["id"].ToString(),
                    id_user = therapy["id_user"].ToString(),
                    name = therapy["name"].ToString(),
                    note = therapy["note"].ToString(),
                    created_at = therapy["created_at"].ToString(),
                    updated_at = therapy["updated_at"].ToString(),
                    recommendation = therapy["recommendation"].ToString()
                };
            }
            return result;
        }
    }
    */
        public class Therapy
        {
            public Therapy(string therapyId)
            {
                //Get therapy info
                string response = Globals.PostRequest(Globals.GetTherapy_ApiUri(therapyId));
                JToken jsonResponse = JToken.Parse(response);
                if (jsonResponse[Globals.TYPE_KEY].ToString() == "ok")
                {
                    JToken therapy = JToken.Parse(jsonResponse[Globals.RESPONSE_KEY].ToString());
                    created_at = therapy["created_at"].ToString();
                    name = "Default Title";
                    string allEntries = Globals.PostRequest(Globals.GetAllEntries_ApiUri(therapyId));
                    JArray allEntriesArray = JArray.Parse(JToken.Parse(allEntries)[Globals.RESPONSE_KEY].ToString());
                    string firstEntry = Globals.PostRequest(Globals.GetEntry_ApiUri(allEntriesArray[0]["id_entry"].ToString()));
                    JToken firstEntryJson = JToken.Parse(firstEntry);
                    specialist =
                        firstEntryJson["specialist"]["surname"].ToString() +
                        firstEntryJson["specialist"]["firstname"].ToString() +
                        firstEntryJson["specialist"]["patronymic"].ToString();
                }

                //Get all entries
                response = Globals.PostRequest(Globals.GetAllEntries_ApiUri(therapyId));
                jsonResponse = JToken.Parse(response);
                if (jsonResponse[Globals.TYPE_KEY].ToString() == "ok")
                {
                    JArray aEntries = (JArray)jsonResponse[Globals.RESPONSE_KEY];
                    foreach (JToken entry in aEntries)
                    {
                        entries.Add(new TherapyEntry(therapyId, entry["id_entry"].ToString()));
                    }
                }
            }
            public string id;
            public string id_user;
            public string name;
            public string note;
            public string created_at;
            public string updated_at;
            public string recommendation;
            public string specialist;
            public List<TherapyEntry> entries;
        }
        public class TherapyEntryShort
        {
            public string id;
            public string id_therapy;
            public string id_entry;
            public string created_at;
            public string updated_at;
            public string name;
            public string note;
            public string id_status;
            public string cost;
        }
        public class TherapyEntryExtended
        {
            public string id;
            public string id_user;
            public string id_cause;
            public string id_specialist;
            public string note;
            public string time_from;
        }
        public class TherapyEntry
        {
            public TherapyEntry(string therapyId, string entryId)
            {
                //Get entry info
                string response = Globals.PostRequest(Globals.GetEntry_ApiUri(entryId));
                JToken jsonResponse = JToken.Parse(response);
                if (jsonResponse[Globals.TYPE_KEY].ToString() == "ok")
                {
                    JToken entry = JToken.Parse(jsonResponse[Globals.RESPONSE_KEY].ToString());

                    documents = GetTherapyDocuments(therapyId);
                }
            }
            public string id;
            public string title;
            public string cost;
            public string date;
            public string specialist;
            public List<TherapyDocument> documents;
            public string comment;
        }
        public class TherapyDocument
        {
            public string uri;
            public string title;
        }
        public static List<TherapyDocument> GetTherapyDocuments(string therapyId)
        {
            List<TherapyDocument> documents = new List<TherapyDocument>();
            string response = Globals.PostRequest(Globals.TherapyFileAll_ApiUri(therapyId));
            JToken jsonResponse = JToken.Parse(response);
            if (jsonResponse[Globals.TYPE_KEY].ToString() == "ok")
            {
                JArray docs = (JArray)jsonResponse[Globals.RESPONSE_KEY];
                foreach (JToken document in docs)
                {
                    TherapyDocument doc = new TherapyDocument()
                    {
                        title = document["name"].ToString(),
                        uri = document["link"].ToString()
                    };
                }
            }
            return documents;
        }
    }
}