﻿using System;
using System.Collections.Specialized;
using System.Net;
using System.Text;
using System.Json;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Diagnostics;

namespace Stomatology
{
    public partial class AuthPage : ContentPage
    {
        public enum Mode
        {
            Login,
            Register
        }
        Mode mode;
        JsonObject response;
        public AuthPage()
        {
            InitializeComponent();
            Globals.CurrentPage = this;
            if (Globals.IsTokenExists)
            {
                mode = Mode.Login;
            }
            else
            {
                mode = Mode.Register;
            }
            if (mode == Mode.Login)
            {
                switchModeLabel.Text = "Еще не зарегистрированны?";
                signButton.Text = "Войти";
                SetVisibility(rePasswordEntry, false, 0, 1);
            }
            signButton.BackgroundColor = Color.White;
            var tgr = new TapGestureRecognizer();
            tgr.Tapped += (s, e) => OnLabelClicked();
            switchModeLabel.GestureRecognizers.Add(tgr);
            phoneEntry.TextChanged += PhoneEntry_TextChanged;
        }
        private void PhoneEntry_TextChanged(object sender, TextChangedEventArgs e)
        {
            /*if (phoneEntry.Text.Length == 0)
            {
                phoneEntry.Text = "+7";
            }*/
        }
        //TODO: async + anim
        public void OnLabelClicked()
        {
            if (mode == Mode.Login)
            {
                switchModeLabel.Text = "Уже зарегистрированны?";
                signButton.Text = "Зарегистрироваться";
                SetVisibility(rePasswordEntry, true, 50, 10);
                mode = Mode.Register;
            }
            else
            {
                switchModeLabel.Text = "Еще не зарегистрированны?";
                signButton.Text = "Войти";
                SetVisibility(rePasswordEntry, false, 50, 10);
                mode = Mode.Login;
            }
        }
        public async void SetVisibility(View view, bool visible, int msPerStep, int steps)
        {
            double alpha = 1.0f;
            double deltaA = alpha / steps;
            if (visible)
            {
                deltaA = -deltaA;
                view.IsEnabled = true;
            }
            for (float i = 0; i < steps; i++)
            {
                await Task.Delay(msPerStep);
                view.Opacity -= deltaA;
            }
            if (!visible)
            {
                view.IsEnabled = false;
            }
        }

        public async void SlideView(View view, Vec2 endPos, int msPerStep, int steps)
        {
            await Task.Delay(msPerStep);
        }
        public void OnClick(object sender, EventArgs e)
        {
            string result = "No result";
            //POST запрос на РЕГИСТРАЦИЮ
            if (mode == Mode.Register)
            {
                if (passwordEntry.Text == rePasswordEntry.Text)
                {
                    NameValueCollection postData = new NameValueCollection
                    {
                        { "phone", phoneEntry.Text },
                        { "password", passwordEntry.Text }
                    };
                    //result = Globals.PostRequest(Globals.RegisterApiUri, postData);

                    result = Globals.PostRequest(Globals.Register_ApiUri, postData);
                    response = (JsonObject)JsonValue.Parse(result);
                    if (response[Globals.TYPE_KEY] == "ok")
                    {
                        Globals.RaiseRegisterHandler();
                    }
                    else
                    {
                        //Костыль
                        try
                        {
                            AlertToast(response[Globals.RESPONSE_KEY]);
                        }
                        catch { }
                    }
                }
                else
                {
                    AlertToast("Пароли не совпадают!");
                }
            }
            //POST запрос на АУТЕНТИФИКАЦИЮ
            else
            {
                NameValueCollection postData = new NameValueCollection
                {
                    { "phone", phoneEntry.Text },
                    { "password", passwordEntry.Text }
                };
                result = Globals.PostRequest(Globals.LogIn_ApiUri, postData);
                response = (JsonObject)JsonValue.Parse(result);
                if (response[Globals.TYPE_KEY] == "ok")
                {
                    Globals.phone = phoneEntry.Text;
                    UpdateToken(response[Globals.ACCESS_TOKEN_KEY]);
                    Globals.RaiseLoginHandler();                    
                }
                else
                {
                    AlertToast(response[Globals.RESPONSE_KEY]);
                }
            }
        }
        public void AlertToast(string alertMessage)
        {
            DisplayAlert("", alertMessage, "OK");
            /*if (Device.OS == TargetPlatform.Android)
            {
                Toast.MakeText(Android.App.Application.Context, alertMessage, ToastLength.Short).Show();
            }
            else if (Device.OS == TargetPlatform.iOS)
            {
                //TODO: iOS notification
            }
            //Debug.Write(alertMessage);
            */
        }

        public void UpdateToken(string token)
        {
            Globals.TOKEN = token;
        }
    }
}
